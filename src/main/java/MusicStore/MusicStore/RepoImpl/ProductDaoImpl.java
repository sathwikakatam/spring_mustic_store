package MusicStore.MusicStore.RepoImpl;

import java.util.List;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import MusicStore.MusicStore.dao.productInterface;
import MusicStore.MusicStore.model.Product;

@Repository
@Transactional
@Component
@Service
public class ProductDaoImpl implements  productInterface{
    
	//System.out.println("hhgsfdadjkshgfdhagjklhgfsahdk");
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public void addProduct(Product product) {
	Session session=sessionFactory.getCurrentSession();
	
	
	 session.saveOrUpdate(product);
	
	}
	@Override
	public void editProduct(Product product) {
		Session session=sessionFactory.getCurrentSession();

		 session.saveOrUpdate(product);
		
		}


	
	
	@Override
	public Product getProductById(int Id) {
		Session session=sessionFactory.getCurrentSession();
		//String hql= "from Product where Product.id = ?" ;
		
		//Query query = session.createQuery(hql).setParameter("id",123);
		//query.setParameter("Id",Id);
		//query.setParameter("Id",id);
		
		  // Product product=(Product)query;
	   Product product=  (Product) session.get(Product.class, Id); 
	    
	     return product;
	}
	
	
	
	@Override
	public List<Product> getAllProducts() {
   	

		Session session=sessionFactory.getCurrentSession();
		 System.out.println("products DA1");
		Query query = session.createQuery("from Product");
	
		 
	   
	    
	     
	     List<Product> products=query.list();
	     
	     return products;
	}

	@Override
	public void deleteproduct(int id) {
		Session session=sessionFactory.getCurrentSession();
         session.delete(getProductById(id));
         session.flush();
         }

}
