package MusicStore.MusicStore.RepoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import MusicStore.MusicStore.dao.AddCartRepo;
import MusicStore.MusicStore.dao.CheckOutRepo;
import MusicStore.MusicStore.dao.productInterface;
import MusicStore.MusicStore.model.Add_cart;
import MusicStore.MusicStore.model.CheckOutCart;
import MusicStore.MusicStore.model.Product;
import MusicStore.MusicStore.model.User;


@Repository
@Transactional
@Component
public class AddToCartRepoImpl  implements AddCartRepo {
	
	@Autowired
	CheckOutRepo checkoutrepo;
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	productInterface pi;
	@Autowired
	AddCartRepo add_rep;
	
	@Override
	public List<Add_cart> getCartByUserId(long userId) {
		Session session=sessionFactory.getCurrentSession();
		Query<Add_cart> query = session.
				createQuery(" Select addCart  FROM AddtoCart add_cart WHERE add_cart.user_id=:user_id", Add_cart.class);
		
		query.setParameter("user_id", userId);
		return query.list();
	}

	@Override
	public List<Add_cart> addCartbyUserIdAndProductId(long productId, long userId, int qty, double price) {
		Session session=sessionFactory.getCurrentSession();
			try {
				
				
				Add_cart obj = new Add_cart();
				obj.setQuantity(qty);
				obj.setCart_id((int)userId);
				Product pro = pi.getProductById((int)productId);
			
				//TODO price has to check with qty
				obj.setPrice(price);
				session.save(obj);		
			}catch(Exception e) {
				e.printStackTrace();
			
				e.getMessage();
			}
			
			return this.getCartByUserId(userId);		}

	@Override
	public void updateQtyByCartId(long cartId, int qty, double price) {
		Session session=sessionFactory.getCurrentSession();
		Query<Add_cart> query = session.
				createQuery("update add_Cart addCart set add_Cart.qty=:qty,"
						+ "add_Cart.price=:price "
						+ "WHERE addCart.id=:cart_id", Add_cart.class);
		query.setParameter("qty", qty);
		query.setParameter("price", price);
		query.setParameter("id", cartId);
		
	}

	@Override
	public List<Add_cart> removeCartByUserId(long parseLong, long parseLong2) {
		Session session=sessionFactory.getCurrentSession();
		Query<Add_cart> query = session.
				createQuery("DELETE  FROM AddtoCart addCart "
						+ "WHERE add_cart.id =:cart_id   and add_cart.user_id=:user_id", Add_cart.class);
		query.setParameter("cart_id", parseLong);
		query.setParameter("addCart.user_id", parseLong2);
		
		return query.list();
	}

	@Override
	public boolean checkTotalAmountAgainstCart(double totalAmt, long user_Id) {
		
		double total_amount =this.getTotalAmountByUserId(user_Id);
		if(total_amount == totalAmt) {
			return true;
		}
		System.out.print("Error from request "+total_amount +" --db-- "+ totalAmount);
		return false;
	
	}

	private double getTotalAmountByUserId(long user_Id) {
		// TODO Auto-generated method stub
		return user_id;
	}

	@Override
	public List<CheckOutCart> saveProductsForCheckout(List<CheckOutCart> tmp) {
		Session session=sessionFactory.getCurrentSession();
		List<CheckOutCart> checkout= new ArrayList<CheckOutCart>();
		try {
			long user_id = tmp.get(0).getUser_id();
			if(tmp.size() >0) {
				checkoutrepo.saveAll(tmp);
				this.removeAllCartByUserId(user_id);
				checkout=		this.getAllCheckoutByUserId(user_id);
			}	
			else {
				throw  new Exception("Should not be empty");
			}
		}catch(Exception e) {
			e.getMessage();
		}
		return checkout;
	}

	private List<CheckOutCart> getAllCheckoutByUserId(long user_id) {
		
		return checkoutrepo.getByuserId(user_id);
	}

	
	public List<Add_cart> removeAllCartByUserId(long userId) {
		AddCartRepo.deleteAllCartByUserId(userId);
		return null;
	}

	@Override
	public Object getCartByProductIdAnduserId(long userId, long productId) {
		// TODO Auto-generated method stub
		return null;
	}

		
	}
	

