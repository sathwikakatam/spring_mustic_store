package MusicStore.MusicStore.RepoImpl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import MusicStore.MusicStore.controller.POJO.SignUp;
import MusicStore.MusicStore.dao.Customers_repository;
import MusicStore.MusicStore.model.Customers;
@Repository
@Transactional
@Component
@Service
public class CustomerRepoImpl implements Customers_repository  {

	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Customers_repository up;
	

	@Override
	public Customers findByMobile(long mobile) {
		Session session=sessionFactory.getCurrentSession();
		String url="select * from customers where phone= :number";
		Query<Customers> query = session.createQuery(url);
		Customers customer =(Customers)query.setParameter("number",mobile);
		return customer;
	}
	
	@Override
	public Customers findById(long id) {
		Session session=sessionFactory.getCurrentSession();
		String url="select * from customers where id= :id ";
		Query query=session.createQuery(url);
		Customers customer=(Customers)query.setParameter("id", id);
		return customer;
	}
	@Override
	public Customers Sign_Up(SignUp signup) {
		Session session=sessionFactory.getCurrentSession();
		Customers customer =new Customers();
		//customer=findByMobile(signup.getPhone());
		try {
			if(customer.equals(null)) {
				throw new Exception("Customer already Exits");
			}
		  customer.setName(signup.getName());
		  customer.setPhone(signup.getPhone());
		  customer.setEmail(signup.getEmail());
		  customer.setId(signup.getId());
		  customer.setPassword(signup.getPassword());
		  session.saveOrUpdate(customer);
		
	}catch(Exception e){
		e.getMessage();
		}
		 return customer;
	}
	
	
}
