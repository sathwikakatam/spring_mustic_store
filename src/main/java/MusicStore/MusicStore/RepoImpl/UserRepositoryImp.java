 package MusicStore.MusicStore.RepoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import MusicStore.MusicStore.dao.UserRepository;
import MusicStore.MusicStore.model.User;

@Repository
@Transactional
@Component
public class UserRepositoryImp implements UserRepository{
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public User findUserByUsername(String username) {
		Session session=sessionFactory.getCurrentSession();
		Query<User> query = session.createQuery(" FROM User where username=:name", User.class);
        query.setParameter("name", username);
      
        System.out.println("The results    "+query.uniqueResult().getName());
        return query.uniqueResult();
		

		
	}

	@Override
	public void insertUser(User user) {
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(user);
		
	}
	
	

}
