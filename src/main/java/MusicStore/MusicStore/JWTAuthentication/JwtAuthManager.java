package MusicStore.MusicStore.JWTAuthentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import MusicStore.MusicStore.JWTAuthentication.UserServices.UserPrinciple;
import MusicStore.MusicStore.controller.POJO.LoginRequest;
import MusicStore.MusicStore.dao.Customers_repository;
import MusicStore.MusicStore.model.Customers;

@Configuration
public class JwtAuthManager {
	@Autowired
	Customers_repository C_repo;
	
	 public Authentication authenticate(UsernamePasswordAuthenticationToken authentication,LoginRequest loginRequest) throws AuthenticationException {
	    String mobile = authentication.getPrincipal() +"";
	    String password = authentication.getCredentials() + "";
	    Customers customer;
		try {
		
			if(C_repo == null) {
				
				 throw new BadCredentialsException("1001");
			}
			customer = C_repo.findByMobile(Long.parseLong(mobile));
			 if (customer == null) {
			        throw new BadCredentialsException("User Not found!!");
			  }
			 
			
			 if(!this.passwordMatch(password, customer.getPassword())) {
				
				 throw new BadCredentialsException("Mobile or password is wrong.");
			 } 

		        return new UsernamePasswordAuthenticationToken(new UserPrinciple
		        		                                 (
		        		                                  Long.parseLong(mobile),
		        		                                  password,
		        		                                  customer.getId(),
		        		                                  customer.getRoles()),
		        		                                 password);
		} catch (Exception e) {
	
			 throw new BadCredentialsException(e.getMessage());
		}
	   
	} 
	 private Boolean passwordMatch(String rawPassword,String from_db_encoded) {
		 return rawPassword.equals(from_db_encoded);
		// return BCrypt.checkpw(rawPassword.toString(),from_db_encoded);	 
	 }

}
