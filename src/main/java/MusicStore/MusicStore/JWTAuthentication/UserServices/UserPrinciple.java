package MusicStore.MusicStore.JWTAuthentication.UserServices;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import MusicStore.MusicStore.model.Customers;
import MusicStore.MusicStore.model.Role;

public class UserPrinciple implements UserDetails {
	private long mobile;
	private String password;
	private long id;
	 private Set<Role>roles;
	
	 public UserPrinciple() {}
	
	public UserPrinciple(long mobile,String password,long id,Set<Role> roles) {
		this.mobile=mobile;
		this.password=password;
		this.id=id;
		this.roles=roles;
		
	}
	
	public UserPrinciple create(Customers user) {
		
		return new UserPrinciple
				      (user.getPhone(),
				    		  user.getPassword(),
				    		  user.getId(),
				    		  user.getRoles());
		
	}
	
	
	public long getId() {
		return id;
	}

	

	public long getMobile() {
		return mobile;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return roles.stream().map(authority -> new SimpleGrantedAuthority(authority.getRole().toString())).collect(Collectors.toList());
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}


	public void setMobile(long mobile) {
		this.mobile = mobile;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	

	public void setId(long id) {
		this.id = id;
	}
	

	

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
