package MusicStore.MusicStore.JWTAuthentication.UserServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import MusicStore.MusicStore.dao.Customers_repository;
import MusicStore.MusicStore.model.Customers;

@Service
public class CustomUserDetailsServices implements UserDetailsService {
	
	@Autowired
	Customers_repository c_service;

	//This method deligates phone and password and authority to the authManager and verifies it
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		try {
           Customers m_customers = c_service.findByMobile(Long.parseLong(username));
            UserPrinciple up = new UserPrinciple().create(m_customers);
            return  new org.springframework.security.core.userdetails.User(String.valueOf(up.getMobile()),
					new BCryptPasswordEncoder().encode(up.getPassword()),
					up.getAuthorities());
	}
	catch(Exception e) {
		throw new UsernameNotFoundException("User Not Found "+username);
		}
	}
	
	
	
	
	//This method is used by the JWT to the user Values based on the JWT
	
	
	public UserDetails findById(Long id) {
		try {
		 Customers m_customers = c_service.findByMobile((id));
         UserPrinciple up = new UserPrinciple().create(m_customers);
         return  new org.springframework.security.core.userdetails.User(String.valueOf(up.getMobile()),
					new BCryptPasswordEncoder().encode(up.getPassword()),
					up.getAuthorities());
	}
	catch(Exception e) {
		throw new UsernameNotFoundException("User Not Found "+id);
		}
	}
	
	

}
