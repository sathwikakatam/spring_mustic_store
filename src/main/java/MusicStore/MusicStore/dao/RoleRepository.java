package MusicStore.MusicStore.dao;

import MusicStore.MusicStore.model.Role;

public interface RoleRepository {
	public Role findByRole(String role);

}
