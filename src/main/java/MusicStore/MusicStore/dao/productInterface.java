package MusicStore.MusicStore.dao;

import java.util.List;

import MusicStore.MusicStore.model.Product;

public interface productInterface {
  void addProduct(Product product);
  Product getProductById(int Id);
  List<Product> getAllProducts();
  void deleteproduct(int id);
void editProduct(Product product);
 
}
