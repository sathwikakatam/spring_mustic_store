package MusicStore.MusicStore.dao;

import MusicStore.MusicStore.model.User;

public interface UserRepository {
  User findUserByUsername(String username);
  void insertUser(User user);
}
