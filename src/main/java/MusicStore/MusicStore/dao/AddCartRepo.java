package MusicStore.MusicStore.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import MusicStore.MusicStore.model.Add_cart;
import MusicStore.MusicStore.model.CheckOutCart;


public interface AddCartRepo  {

	List<Add_cart> getCartByUserId(long userId);

	List<Add_cart> addCartbyUserIdAndProductId(long productId, long userId, int qty, double price);

	void updateQtyByCartId(long cartId, int qty, double price);

	List<Add_cart> removeCartByUserId(long parseLong, long parseLong2);

	boolean checkTotalAmountAgainstCart(double total_amt, long user_Id);

	List<CheckOutCart> saveProductsForCheckout(List<CheckOutCart> tmp) throws Exception;

	static void deleteAllCartByUserId(long userId) {
		// TODO Auto-generated method stub
		
	}

	Object getCartByProductIdAnduserId(long userId, long productId);


	



	

}
