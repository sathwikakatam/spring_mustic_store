package MusicStore.MusicStore.dao;

import java.util.HashMap;
import java.util.Optional;
import MusicStore.MusicStore.controller.POJO.SignUp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import MusicStore.MusicStore.model.Customers;
import MusicStore.MusicStore.model.User;

@Component
public interface Customers_repository  {
	//public Customers findByMobile(String mobile);
	Customers findByMobile(long mobile);
	Customers findById(long id);
	Customers Sign_Up(SignUp signup);
}
