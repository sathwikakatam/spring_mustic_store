package MusicStore.MusicStore.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import MusicStore.MusicStore.JWTAuthentication.JwtAuthenticationEntryPoint;
import MusicStore.MusicStore.JWTAuthentication.JwtAuthenticationFilter;

@ComponentScan({"MusicStore.MusicStore.config"})
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {
	//If any exception occurs then it goes to this point
	@Autowired
	JwtAuthenticationEntryPoint  entrypoint;

	@Autowired
	UserDetailsService userdetailsService;
	
	//@Autowired
	//CustomAuthenticationSucessHandler customauthenticationsucesshandler;
	
	
	//verifying if the JWt Token
	
	@Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }
	//password Encoder

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
//	
//	@Bean
//	public DaoAuthenticationProvider authenticationProvider() {
//		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
//		authProvider.setUserDetailsService(userdetailsService);
//		authProvider.setPasswordEncoder(passwordEncoder());
//	
//		return authProvider;
	//}
	
	
	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	@Override
	 public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
	
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    auth.userDetailsService(userdetailsService).passwordEncoder(passwordEncoder());
		//auth.inMemoryAuthentication().withUser("sathwika").password("sathwika").roles("ADMIN");
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.
		    csrf()
		    .disable()
		    .exceptionHandling()
		    .authenticationEntryPoint(entrypoint)
		    .and()
		    .sessionManagement()
		    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		    .and()
		    .authorizeRequests()	
		   	.antMatchers("/admin").hasRole("ADMIN")
		   //.anyRequest().authenticated()
     	     .antMatchers("/cart").hasRole("USER")
     	     .antMatchers("/","productList","/rest/login","rest/signUp").permitAll()
     	     .anyRequest().authenticated().and()
		    //.formLogin().loginPage("/login")
	       // .loginProcessingUrl("/doLogin")
		    //.successHandler(customauthenticationsucesshandler)
		    //.failureUrl("/login?error").and()
		   //.logout().logoutUrl("/doLogout").logoutSuccessUrl("/log?logout").permitAll()
		    
		    .exceptionHandling().accessDeniedPage("/error");
		 http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

	}
}