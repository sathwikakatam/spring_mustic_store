package MusicStore.MusicStore.config;
//import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

	@Configuration
	@EnableTransactionManagement
	@ComponentScan({ " MusicStore.MusicStore.config" })
	public class hibernateConfig {
	    @Autowired
	    private Environment environment;
	 
	    @Bean
	    public LocalSessionFactoryBean sessionFactory() {
	        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	        sessionFactory.setDataSource(dataSource());
	        sessionFactory.setPackagesToScan(new String[] { "MusicStore.MusicStore.model" });
	        System.out.println("table was created");
	        sessionFactory.setHibernateProperties(hibernateProperties());
	        return sessionFactory;
	     }
	    @Bean(name="datasource")
	    public DataSource dataSource() {
	        DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
	        dataSource.setUrl("jdbc:mysql://localhost:3306/Music_Stores?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true");//serverTimezone=UTC&useSSL=false
	       // dataSource.setUrl("jdbc:mysql://localhost:3306/customers?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true");
	        dataSource.setUsername("root");
	        dataSource.setPassword("Sairam@1");
	       
	        return dataSource;
	    }
	    private Properties hibernateProperties() {
	        Properties properties = new Properties();
	     
	        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
	        properties.put("hibernate.show_sql", "true");
	        properties.put("hibernate.format_sql","true");
	        properties.put("hibernate.hbm2ddl.auto","update");
	        return  properties;  
	    
	        

	    }
	    @Bean
	    @Autowired
	    public HibernateTransactionManager transactionManager(SessionFactory s) {
	       HibernateTransactionManager txManager = new HibernateTransactionManager();
	       txManager.setSessionFactory(s);
	       return txManager;
	    }
	    
	    @Bean
	    public MultipartResolver multipartResolver() {
	       CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
	       multipartResolver.setMaxUploadSize(10485760); 
	       return multipartResolver;
	    }
	   
	  

	}


