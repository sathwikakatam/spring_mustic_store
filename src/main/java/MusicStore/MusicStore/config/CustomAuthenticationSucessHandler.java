package MusicStore.MusicStore.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import MusicStore.MusicStore.dao.UserRepository;
import MusicStore.MusicStore.model.User;
@Component
public class CustomAuthenticationSucessHandler implements AuthenticationSuccessHandler{
	@Autowired
	UserRepository userrepo;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		String userName = authentication.getName();
		User theUser = userrepo.findUserByUsername(userName);
		//create session
		HttpSession session = request.getSession();
		session.setAttribute("user", theUser);
		
		//forward it to the login page  to direct to roles
		
		response.sendRedirect(request.getContextPath()+"/roles");
		
		
	}

}
