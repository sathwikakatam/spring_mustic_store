package MusicStore.MusicStore.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import MusicStore.MusicStore.RepoImpl.ProductDaoImpl;
import MusicStore.MusicStore.dao.AddCartRepo;
import MusicStore.MusicStore.model.Add_cart;
import MusicStore.MusicStore.model.CheckOutCart;

@RestController
@RequestMapping("api/order")
public class OrderController {
	@Autowired
	AddCartRepo cartService;
	@Autowired
	ProductDaoImpl proService;
	
	@RequestMapping("checkout_order")
  	public ResponseEntity<?> checkout_order(@RequestBody HashMap<String,String> addCartRequest) {
		try {
			long user_Id = Long.parseLong(addCartRequest.get("userId"));
			double total_amt = Double.parseDouble(addCartRequest.get("total_price"));
			if(cartService.checkTotalAmountAgainstCart(total_amt,user_Id)) {
				List<Add_cart> cartItems = cartService.getCartByUserId(user_Id);
				List<CheckOutCart> tmp = new ArrayList<CheckOutCart>();
				for(Add_cart addCart : cartItems) {
					String orderId = ""+getOrderId();
					CheckOutCart cart = new CheckOutCart();
					cart.setPayment_type(addCartRequest.get("pay_type"));
					cart.setPrice(total_amt);
					cart.setUser_id(user_Id);
					cart.setOrder_id(orderId);
					cart.setProduct(addCart.getProduct());
					cart.setQty(addCart.getQuantity());
					cart.setDelivery_address(addCartRequest.get("deliveryAddress"));
					tmp.add(cart);
				}
				cartService.saveProductsForCheckout(tmp);
				return ResponseEntity.ok("OK");
			}else {
				throw new Exception("Total amount is mismatch");
			}
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body("bad Request");
		}
	}
	public int getOrderId() {
	    Random r = new Random( System.currentTimeMillis() );
	    return 10000 + r.nextInt(20000);
	}
	@RequestMapping("getOrdersByUserId")
		public ResponseEntity<?> getOrdersByUserId(@RequestBody HashMap<String,String> ordersRequest) {
		try {
			String keys[] = {"userId"};	
			return ResponseEntity.ok("Ok");
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body("badRequest");
		}
		
	}
}