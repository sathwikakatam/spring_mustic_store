package MusicStore.MusicStore.controller.POJO;

public class LoginRequest {
	private long mobile;
	private String password;
	public long getMobile() {
		return mobile;
	}
	public void setMobile(long mobile) {
		this.mobile = mobile;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
