package MusicStore.MusicStore.controller;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import MusicStore.MusicStore.JWTAuthentication.JwtAuthManager;
import MusicStore.MusicStore.JWTAuthentication.JwtAuthenticationProvidor;
import MusicStore.MusicStore.JWTAuthentication.UserServices.CustomUserDetailsServices;
import MusicStore.MusicStore.JWTAuthentication.UserServices.UserPrinciple;
import MusicStore.MusicStore.controller.POJO.LoginRequest;
import MusicStore.MusicStore.dao.Customers_repository;
import MusicStore.MusicStore.model.Customers;

@RequestMapping("api")
public class Log_InController {
	
	@Autowired
	CustomUserDetailsServices c_service;
	
	@Autowired
	Customers_repository c_repo;
	
	@Autowired
	JwtAuthenticationProvidor provider;
	
	@Autowired
   JwtAuthManager manager;
	
	//Getting the user details using jwt to send Client
	      @RequestMapping("/rest/logIn")
	  public ResponseEntity<?> userLogin(@RequestBody LoginRequest loginrequest){
	    	  //authenticate Results by using AuthManager
	    	  try {
	    Authentication authentication = manager.authenticate
	    		                (new UsernamePasswordAuthenticationToken
	    		                 (loginrequest.getMobile(),loginrequest.getPassword()), loginrequest);
	     SecurityContextHolder.getContext().setAuthentication(authentication);
	    	 
	     //GenerateToken by tokenprovider   
	     String token = provider.generateToken(authentication);
	     
	     //geting JSON object that has userdetails and token
	     JSONObject obj = generateUserdetails(token); 
	     
	    //check if the Obj is empty
	     
	    	 if(obj==null) {
	    		 throw  new Exception("Error while generating Reponse");	    	 
	     }
	    	 return new ResponseEntity<String>(obj.toString(),HttpStatus.OK);
	    	  }catch(Exception e) {
	    		
	      		return ResponseEntity.badRequest().body("Authentication Failed");

	    	  }	  
	      }

	   
	  	//Generating UserDetails adding Json Token to it and retiuring Json Object  
	    
	      public JSONObject generateUserdetails(String token) {
	    	    //create a Map to store the values
	    	  Map<String,String>userDetails = new HashMap();  
	    	  //get the user id from getuserId method
	    	  
	    	  Customers customer  = c_repo.findById(this.getuserId());  
	    	  
	    	  //get the userdetails and put in List
	    	  
	    	    userDetails.put("User_id",String.valueOf(this.getuserId()));
	    	    userDetails.put("name", customer.getName());
	    	    userDetails.put("Email", customer.getEmail());
	    	    userDetails.put("mobile",String.valueOf(customer.getPhone()));
	    	    
	    	  //add both the user Details List and token to the Json Object
	    	    JSONObject jobj =new JSONObject();
	    	    jobj.put("Userdetails", userDetails);
	    	    jobj.put("token", token);
	    	  
	    	    //return JsonObject
	    	    return jobj;
	    	  
	      }
	  	
	
	

	
	//Generating UserID
	   public Long getuserId() {
	   UserPrinciple user= 	  (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	   return user.getId();
	   
	   }
	
	
	
	
	
	
	

}
