package MusicStore.MusicStore.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import MusicStore.MusicStore.dao.RoleRepository;
import MusicStore.MusicStore.dao.UserRepository;
import MusicStore.MusicStore.model.User;
//import MusicStore.MusicStore.service.M_userdetails;

@Controller
public class LogInController {
	
	@Autowired
	UserRepository usd;
	
	@Autowired
	RoleRepository rsd;


	@RequestMapping(value="/login")
	public String login(@RequestParam(value="error", required=false) String error,
			            @RequestParam(value="logout",required=false)String logout,Model model){
		
		if(error!=null) {
			model.addAttribute("err","Please enter valid credentials");
		}
	
		else if(logout!=null) {
			model.addAttribute("msg","you are logged out successfully");
		}
		
		return "Login";
	}
	
	
	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public String dashboard(Model theModel, HttpServletRequest request) {
	    HttpSession session = request.getSession();
		User loggedInuser = (User)session.getAttribute("user");
		if(loggedInuser==null) {
	    return "redirect:/login";
		}
		theModel.addAttribute("loggedInUser", loggedInuser);
		return "roles";
	}
	
	
	
//	 @RequestMapping(value = "/roles", method = RequestMethod.GET)
//	    public String postLogin(Model model, HttpSession session) {
//	        
//
//	        // read principal out of security context and set it to session
//	        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
//	        validatePrinciple(authentication.getPrincipal());
//	        User loggedInUser = ((M_userdetails) authentication.getPrincipal()).getUserDetails();
//
//	        model.addAttribute("currentUser", loggedInUser.getName());
//	        session.setAttribute("userId", loggedInUser.getId());
//	        return "roles";
//	    }
//	 
//	 
//	 private void validatePrinciple(Object principal) {
//	        if (!(principal instanceof M_userdetails)) {
//	            throw new  IllegalArgumentException("Principal can not be null!");
//	        }
//	    }
	
	
	
//	@RequestMapping(value="/roles",method=RequestMethod.GET)
//	public String roles() {
//		
//	}
	
	
	 

}
