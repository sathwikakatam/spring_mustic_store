package MusicStore.MusicStore.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import MusicStore.MusicStore.dao.productInterface;
import MusicStore.MusicStore.model.Product;

@Controller
public class AdminController {
	
	Path path;
	  @Autowired
	    ServletContext servletContext;
	  
	  //private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
   productInterface productDaoImpl;
	 @RequestMapping("/admin")
	    public String admin() {
	        return "admin";
	    }
	    @RequestMapping("/admin/productInventory")
	    public String productInventory( Model model ) throws IOException {
	    	   List<Product> products = productDaoImpl.getAllProducts();
	    	   // Product products=productDaoImpl.getProductById("123");
	    	       model.addAttribute("product",products);
	        return "productInventory";
	    }
	    @RequestMapping("/admin/productInventory/addcart")
	    public String addProduct(Model model) {
	        Product product = new Product();
	        product.setProductCategory("instrument");
	        product.setProductCondition("new");
	        product.setProductStatus("active");

	        model.addAttribute("product", product);

	        return "addcart";
	    }
	    @RequestMapping(value="/admin/productInventory/addcart", method = RequestMethod.POST)
	    public String productInventoryPost( @Valid @ModelAttribute("product")  Product product,BindingResult result,HttpServletRequest request) throws IOException {
	    	
	    	if(result.hasFieldErrors()) {
	    		return "addcart";
	    	}
	    	
	    	
	    	//String pat = System.getProperty("user.dir");
	    	//System.out.println(pat);
	    	 productDaoImpl.addProduct(product);
	   	MultipartFile productimage=product.getProductImage();    	
//	  	 String rootDirectory = request.getSession().getServletContext().getRealPath("/");         
//	  	 path = Paths.get(rootDirectory + "WEB-INF\\resources\\images\\"+productimage.getOriginalFilename())
	   	System.out.println(productimage!= null);
	    	if (productimage!= null && !productimage.isEmpty()) {
	      
	            String fileName = servletContext.getRealPath("/") + "\\WEB-INF\\resources\\images\\" + product.getId()+".png";
	            try {
	            	
	                productimage.transferTo(new File(fileName));
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    	else {
	    		// System.out.println("Entered into If");
	    	}
	    		 
	       return "redirect:/admin/productInventory";
	    }
	    
	    @RequestMapping(value="/admin/productInventory/{productId}")
	    public String deleteProduct(@PathVariable("productId") int productId,HttpServletRequest request) {
	    	productDaoImpl.deleteproduct(productId);
	    	 String rootDirectory = request.getSession().getServletContext().getRealPath("/");
	         path = Paths.get(rootDirectory + "\\WEB-INF\\resources\\images\\"+productId+".png");
	    	return "redirect:/admin/productInventory";
	}
	    
	    @RequestMapping(value="admin/productInventory/update/{productId}")
	    public String updateProduct(@PathVariable("productId") String productId,Model model) {
	    Product product=	productDaoImpl.getProductById(Integer.parseInt(productId));
	    	model.addAttribute(product);
	    	return "update";
	    }
	    
	    @RequestMapping(value="/admin/productInventory/editProduct",method=RequestMethod.POST)
	    public String updating( @ModelAttribute("product")  @Valid Product product,BindingResult result,HttpServletRequest request) {
	    
	   
	    	if(result.hasErrors()) {
	    		
	    		return "update";
	    	}
	    	MultipartFile productimage=product.getProductImage();
	    	if(productimage!=null && !productimage.isEmpty()) {
	    		try {
	    		String root=request.getSession().getServletContext().getRealPath("/")+"\\WEB-INF\\resources\\images\\"+product.getId()+".png";
	    		
	    		productimage.transferTo(new File(root));
	    	}
	    	catch(IOException e)	{
	    		e.printStackTrace();
	    		
	    			}
	    		}
	    	productDaoImpl.editProduct(product);
	    	return "redirect:/admin/productInventory";
	    }
	}


