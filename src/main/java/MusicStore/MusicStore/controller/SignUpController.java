package MusicStore.MusicStore.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import MusicStore.MusicStore.controller.POJO.SignUp;
import MusicStore.MusicStore.dao.Customers_repository;
import MusicStore.MusicStore.model.Customers;

@RestController
@RequestMapping("/signUp")
public class SignUpController {
	
	@Autowired
	Customers_repository r_customer;
	
	//create link
	@RequestMapping("/user")
	public ResponseEntity<?> signUp(@RequestBody SignUp signup) {
		try {
		Customers customer=r_customer.Sign_Up(signup);
		
		if(customer==null) {
			throw new Exception("unable to create account"); 
		}
		return ResponseEntity.ok(customer);
	}
		catch(Exception e) {
		   return ResponseEntity.badRequest().body("Something went wrong while Signing Up");
	}
	}

	
	//create signup method
	
	//send into repo
	
	//return httstatus with Json Data and Ok
	
	

}
