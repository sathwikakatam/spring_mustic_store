package MusicStore.MusicStore.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import MusicStore.MusicStore.dao.productInterface;
import MusicStore.MusicStore.model.Product;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	productInterface product;
	
	
	@RequestMapping("/getAll")
	public List<Product> getAllProducts(){
		return product.getAllProducts();

}
}