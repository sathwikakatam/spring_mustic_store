package MusicStore.MusicStore.controller;

import java.io.File;
import java.io.IOException;
import java.net.http.HttpClient.Redirect;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.validation.Validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

//import MusicStore.MusicStore.dao.ProductDaoImpl;
import MusicStore.MusicStore.dao.productInterface;
//import MusicStore.MusicStore.dao.productdaoImpl.ProductDaoImpl;
import MusicStore.MusicStore.model.Product;
//import MusicStore.MusicStore.model.ProductDaoImpl;


@Controller

public class HomeController {
	Path path;
	  @Autowired
	    ServletContext servletContext;
	  
	  //private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
     productInterface productDaoImpl;

	@RequestMapping(value="/")
	public ModelAndView test(HttpServletResponse response) throws IOException{
		return new ModelAndView("home");
	}
	
	

    @RequestMapping("/productList")
    public String getProducts(Model model) {
    	 
     List<Product> products = productDaoImpl.getAllProducts();
   // Product products=productDaoImpl.getProductById("123");
       model.addAttribute("product",products);

        return "productList";
    }
    @RequestMapping("/productList/productDetails/{productId}")
    public String viewProducts(@PathVariable("productId") String productId, Model model ) throws IOException {
    	
    	
    	Product product=productDaoImpl.getProductById(Integer.parseInt(productId));
    	//System.out.println(product.getId());
       model.addAttribute(product);

        return "productView";
    }
}
   