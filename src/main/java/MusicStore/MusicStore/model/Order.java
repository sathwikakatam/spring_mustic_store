package MusicStore.MusicStore.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="order")
public class Order {
	
@Id	
int order_id;
String order_id_id;
int product_Id;
int quantity;
double price;
int status;
public int getOrder_id() {
	return order_id;
}
public void setOrder_id(int order_id) {
	this.order_id = order_id;
}
public String getOrder_id_id() {
	return order_id_id;
}
public void setOrder_id_id(String order_id_id) {
	this.order_id_id = order_id_id;
}
public int getProduct_Id() {
	return product_Id;
}
public void setProduct_Id(int product_Id) {
	this.product_Id = product_Id;
}
public int getQuantity() {
	return quantity;
}
public void setQuantity(int quantity) {
	this.quantity = quantity;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public int getStatus() {
	return status;
}
public void setStatus(int status) {
	this.status = status;
}

}
