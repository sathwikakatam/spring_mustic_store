package MusicStore.MusicStore.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;


/**
 * Created by Le on 1/2/2016.
 */

@Entity
@Component

@Table(name="Product")
public class Product  {

	
	
       @Id
       @Column
       @GeneratedValue(strategy=GenerationType.AUTO)
	  private int id;
	@NotEmpty(message="Please enter productname")
    private String productName;
    @Column
    private String productCategory;
    @Column
    private String productDescription;
   @Min(value=1, message="Price should be greater than 1")
    private double productPrice;
    @Column
    private String productCondition;
    @Column
    private String productStatus;
    @Min(value=1, message="Price should be greater than 1")
    private int unitIntStock; 
    @Column
    private String productManufacturer;

    @Transient
    private MultipartFile productImage;
    
    public int getId() {
  		return id;
  	}
    
    public void setId(int id) {
		this.id = id;
	}
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductCondition() {
        return productCondition;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public int getUnitInStock() {
        return unitIntStock;
    }

    public void setUnitInStock(int unitInStock) {
        this.unitIntStock = unitInStock;
    }

    public String getProductManufacturer() {
        return productManufacturer;
    }

    public void setProductManufacturer(String productManufacturer) {
        this.productManufacturer = productManufacturer;
    }

    public MultipartFile getProductImage() {
        return productImage;
    }

    public void setProductImage(MultipartFile productImage) {
        this.productImage = productImage;
    }
}
