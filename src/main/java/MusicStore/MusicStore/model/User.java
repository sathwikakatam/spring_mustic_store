package MusicStore.MusicStore.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	
	@Column(name="username",nullable=false,unique=true)
	private String name;
	
	
	@Column(name="password",nullable=false)
	private String password;
	
	
	/*@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_authority",
            joinColumns = @JoinColumn(
            		name = "user_id",referencedColumnName = "id") ,
            inverseJoinColumns = @JoinColumn(
            		name = "authority_id",referencedColumnName = "id"))*/

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_authority",
            joinColumns = {@JoinColumn(name = "user_id")} ,
            inverseJoinColumns = {@JoinColumn(name = "authority_id")})
	private Set<Role> roles = new HashSet<>();
	
	User(){}
	User(String name,String password,Set<Role>roles){
		this.name=name;
		this.password=password;
		this.roles=roles;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	
	

}
