package MusicStore.MusicStore.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="add_cart")
public class Add_cart {
	@Id
   int cart_id;
   Product product;
   int quantity;
   double price;
   String added_date;
public int getCart_id() {
	return cart_id;
}
public void setCart_id(int cart_id) {
	this.cart_id = cart_id;
}
public Product getProduct() {
	return product;
}
public void setProduct_Id(int product_Id) {
	this.product = product;
}
public int getQuantity() {
	return quantity;
}
public void setQuantity(int quantity) {
	this.quantity = quantity;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public String getAdded_date() {
	return added_date;
}
public void setAdded_date(String added_date) {
	this.added_date = added_date;
}

	
	
}
