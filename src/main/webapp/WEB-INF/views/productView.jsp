<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Carousel Template for Bootstrap</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">


</head>
<!-- NAVBAR
================================================== -->
<body>
	<%@ include file="/WEB-INF/views/Templates/header.jsp"%>


	<div class="container-wrapper">
		<div class="container">
			<div class="jumbotron">
				<h1>Product Name</h1>

			</div>
			</div>
			<!-- On rows -->
			<div class="container">
				<div class="row">
					<div class="col-md-4"><img src="" href=""/></div>
					<div class="col-md-8">
					
					 <h3>${product.productName }</h3>
					 <h6>Product description:    ${product.productCategory } </h6>
					 <h6>Manufacturer:     ${product.productManufacturer }</h6>
					 <h6>Category:          ${product.productCategory }</h6>
					 <h6>Price:             ${product.productPrice}</h6>
					 </div>
					 </div>
					 </div>
					 </div>
					


				<!-- FOOTER -->
				<%@ include file="/WEB-INF/views/Templates/footer.jsp"%>
			

	</body>
</html>	

		<!-- /.container -->
