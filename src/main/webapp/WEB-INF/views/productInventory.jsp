<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Carousel Template for Bootstrap</title>

<!-- Bootstrap core CSS -->
     <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">


</head>
<!-- NAVBAR
================================================== -->
<body>
	<%@ include file="/WEB-INF/views/Templates/header.jsp"%>



	<div class="container-wrapper">
		<div class="container">
			<div class="jumbotron">
				<h1>All Products</h1>
				<p>Check out Amazing products available</p>
			</div>
			<!-- On rows -->
			<table class="table table-striped table-bordered">
				<thead>
					<th>Product</th>
					<th>Name</th>

					<th class="table-primary">Category</th>


					<th class="table-danger">Condition</th>
					<th class="table-success">Price</th>
					<th>Product Info</th>
					<th>remove</th>

				</thead>

				<c:forEach var="prods" items="${product}">

					<tr>

						<!-- On cells (`td` or `th`) -->

						<td><img src="" href=""></td>

						<td>${prods.productName}</td>

						<td>${prods.productCategory}</td>


						<td>${prods.productCondition }</td>
						<td>${prods.productPrice }USD</td>

						<td><a
							href="<c:url value="/productList/productDetails/${prods.id}"/>"><span
								class="glyphicon glyphicon-info-sign"></a></span>
					</td>
								<td>
								
						
								<a
								
							href="<c:url value="/admin/productInventory/${prods.id}"/>"><span
								class="glyphicon glyphicon-remove"></a></span></td>
								</td>
							
							<td>	<a href="<c:url value="/admin/productInventory/update/${prods.id}"/>"><span
								class="glyphicon glyphicon-pencil"></a></span></td>
									

					</tr>
				</c:forEach>
			</table>
			<a href="<c:url value="/admin/productInventory/addcart"/>" btn
				class="btn btn-primary"> add Product</a>
			<!-- FOOTER -->
			<%@ include file="/WEB-INF/views/Templates/footer.jsp"%>
		</div>

	</div>

	<!-- /.container -->


</body>
</html>


