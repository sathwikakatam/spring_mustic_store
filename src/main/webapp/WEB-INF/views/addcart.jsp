<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Carousel Template for Bootstrap</title>

<!-- Bootstrap core CSS -->
     <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">



</head>
<body>
	<%@ include file="/WEB-INF/views/Templates/header.jsp"%>
	

   <h1>Add Product</h1>
   
 
 <form:form action="${pageContext.request.contextPath}/admin/productInventory/addcart" 
                   modelAttribute="product" method="post" enctype = "multipart/form-data" >  
          <div class="form-group">
            <label for="name">Name</label> <form:errors path="productName" cssStyle="color:#ff0000"></form:errors>
            <form:input type="text" path="productName" id="name" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="category">Category</label>
            <label class="checkbox-inline">
                 <form:radiobutton  class="form-check-input"  path="productCategory" id="category"
                                                             value="instrument" name="productCategory"/>Instrument</label>
            <label class="checkbox-inline"><form:radiobutton class="form-check-input"  path="productCategory" id="category"
                                                             value="record" name="productCategory"/>Record</label>
            <label class="checkbox-inline"><form:radiobutton class="form-check-input" path="productCategory" id="category"
                                                             value="accessory" name="productCategory"/>Accessory</label>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <form:textarea path="productDescription" id="description" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="price">Price</label><form:errors path="productPrice" cssStyle="color:#ff0000"></form:errors>
            <form:input  path="productPrice" id="price" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="condition">Condition</label>
            <label class="checkbox-inline"><form:radiobutton class="form-check-input"  path="productCondition" id="condition"
                                                     name="condition"        value="new"  /><span>New</span></label>
            <label class="checkbox-inline"><form:radiobutton class="form-check-input"  path="productCondition" id="condition"
                                                       name="condition"          value="used" />Used</label>
        </div>

        <div class="form-group">
            <label for="status">Status</label>
            <label class="checkbox-inline"><input type=radio path="productStatus" id="status"
                                                       name= "productStatus"     value="active" />Active</label>
            <label class="checkbox-inline"><input type=radio path="productStatus" id="status"
                                                      name= "productStatus"         value="inactive" />Inactive</label>
        </div>

        <div class="form-group">
            <label for="unitInStock">Unit In Stock</label> <form:errors path="unitInStock" cssStyle="color:#ff0000"></form:errors>
            <form:input  path="unitInStock" id="unitInStock" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="manufacturer">Manufacturer</label>
            <form:input path="productManufacturer" id="manufacturer" class="form-Control"/>
        </div>

        <div class="form-group">
            <label class="control-label" for="productImage">Upload Picture</label>
            <form:input id="productImage" path="productImage" type="file" class="form:input-large" />
        </div>


        <br><br>
        <input type="submit" value="submit" class="btn btn-default">
        <a href="<c:url value="/admin/productInventory" />" class="btn btn-default">Cancel</a>
        </form:form>
         
   
   
  
	

				<!-- FOOTER -->
				 <%@ include file = "/WEB-INF/views/Templates/footer.jsp"%>
			

</body>
</html>